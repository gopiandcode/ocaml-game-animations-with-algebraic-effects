open Containers

module Cell = struct

  type state = Static | Growing

  type data = { id: Id.t;  text: string; }
  let to_pair {id;text} = (id,text)
  let compare_data a b = Ord.pair Id.compare String.compare (to_pair a) (to_pair b)
  let equal_data a b = Equal.pair Id.equal String.equal (to_pair a) (to_pair b)

  type t = { data: data; mutable x: int; mutable y: int;
             mutable width: int; mutable height: int; mutable state:state; }

  let id t =  t.data.id
  let compare {data=a;_} {data=b;_} = compare_data a b
  let equal {data=a;_} {data=b;_} = equal_data a b

  let contains cell (x,y) =
    let min_x = (cell.x - cell.width/2) in
    let max_x = (cell.x + cell.width/2) in
    let min_y = (cell.y - cell.height/2) in
    let max_y = (cell.y + cell.height/2) in
    (min_x <= x) && (x <= max_x) && (min_y <= y) && (y <= max_y)

  let draw cs cell =
    let (x,y) = Coordinate_system.from_world cs (cell.x, cell.y) in
    Graphics.set_color (Graphics.rgb 100 100 100);
    Graphics.fill_rect (x - cell.width/2) (y - cell.height/2) cell.width cell.height;
    match cell.state with
    | Static ->
      let (tx,ty) = Graphics.text_size cell.data.text in
      Graphics.moveto (x - tx/2) (y - ty/2);
      Graphics.set_color Graphics.white;
      Graphics.draw_string cell.data.text
    | Growing -> ()
end

module G = Graph.Imperative.Digraph.Concrete (struct
    type t = Cell.t
    let compare = Cell.compare
    let equal = Cell.equal
    let hash = Hash.poly
  end)

type state = View | Move of Cell.t | EdgeStart | EdgeFirst of Cell.t | Insert | Debounce

type t = G.t * (state ref)

let id = Id.next_id ()

module GridAnim = struct type t = unit  let id () = id end

let debounce_to state ~stop ~delay = Animation.build @@ fun () -> 
  let open Animation in
  state := Debounce;
  run Transition.(Delay.of_ ~delay);
  state := stop;
  return Transition.identity

let strings = ["An arbitrary string"; "Another random string"; "Some piece of text"; "Another piece of text"; "More text"; "This text is randomly generated"; "Generated random strings"; "strings randomly generated"; "This was written in Ocaml"; "Animated graph viewer"]

let draw cs ((graph, state): t)  =
  Graphics.set_line_width 10;
  Graphics.set_color Graphics.red;
  G.iter_edges (fun c1 c2 ->
      let c1 = Coordinate_system.from_world cs (c1.x, c1.y) in
      let c2 = Coordinate_system.from_world cs (c2.x, c2.y) in
      Fun.uncurry Graphics.moveto c1;
      Fun.uncurry Graphics.lineto c2;
    ) graph;
  begin match !state with
    | EdgeFirst cell ->
      let screen_mouse_pos = (Graphics.mouse_pos ()) in
      let end_point = Coordinate_system.from_world cs (cell.x, cell.y) in
      Fun.uncurry Graphics.moveto end_point;
      Fun.uncurry Graphics.lineto screen_mouse_pos
    | _ -> ()
  end;
  G.iter_vertex (Cell.draw cs) graph;
  match !state with
  | View ->
    Graphics.set_color Graphics.blue;
    Graphics.moveto 0 0;
    Graphics.draw_string "View mode"
  | Move _ -> 
    Graphics.set_color Graphics.blue;
    Graphics.moveto 0 0;
    Graphics.draw_string "Move mode"
  | Insert ->
    Graphics.set_color Graphics.blue;
    Graphics.moveto 0 0;
    Graphics.draw_string "Insert mode"
  | EdgeStart | EdgeFirst _ ->
    Graphics.set_color Graphics.blue;
    Graphics.moveto 0 0;
    Graphics.draw_string "Insert Edge mode"
  | Debounce -> ()


let create_animation (cell: Cell.t)  = Animation.build @@ fun () -> 
  let open Animation in
  cell.state <- Growing;
  run @@
    Transition.(Combine.in_parallel
      (InterpolateInt.between ~start:0 ~stop:200 ~delay:200 (fun n -> cell.width <- n))
      (InterpolateInt.between ~start:0 ~stop:100 ~delay:200 (fun n -> cell.height <- n)));
  cell.state <- Cell.Static;
  return Transition.identity

let create_cell (x,y) txt =
  let cell = Cell.{data={id=Id.next_id(); text=txt}; x;y; width=0; height=0; state=Static} in
  Animation_manager.add_animation (module Cell) cell (create_animation cell);
  cell

let update cs ((graph, state): t) (s: Graphics.status) _time =
  let set_to_state st = Animation_manager.add_animation (module GridAnim) ()
      (debounce_to state ~stop:st ~delay:100) in
  match !state with
  | Debounce -> true
  | EdgeStart ->
    let screen_mouse_pos = Coordinate_system.from_display cs (s.mouse_x,s.mouse_y) in
    let cell_contains = Fun.flip G.iter_vertex graph |> Iter.find_pred (Fun.flip Cell.contains screen_mouse_pos) in
    begin match cell_contains, Lazy.from_fun Graphics.button_down  with
      | Some cell, lazy true ->
        set_to_state (EdgeFirst cell);
        true
      | _ -> false
    end
  | EdgeFirst cell ->
    let screen_mouse_pos = Coordinate_system.from_display cs (s.mouse_x,s.mouse_y) in
    let cell_contains = Fun.flip G.iter_vertex graph |> Iter.find_pred (Fun.flip Cell.contains screen_mouse_pos) in
    begin match
        cell_contains, Lazy.from_fun Graphics.button_down, Lazy.from_fun Graphics.key_pressed, Lazy.from_fun Graphics.read_key
      with
      | Some other_cell, lazy true, _, _ ->
        if not (Cell.equal cell other_cell) && not (G.mem_edge graph cell other_cell)
        then (
          set_to_state View;
          G.add_edge graph cell other_cell
        )
        else ();
        true
      | _, lazy false, _, _ -> set_to_state EdgeStart; true
      | _, _, lazy true, lazy 'e' -> set_to_state View; true
      | _, _, lazy true, lazy 'i' -> set_to_state Insert; true
      | _, lazy true, _, _ -> true
    end
  | Insert -> 
    let screen_mouse_pos = Coordinate_system.from_display cs (s.mouse_x,s.mouse_y) in
    let cell_contains = Fun.flip G.iter_vertex graph |> Iter.find_pred (Fun.flip Cell.contains screen_mouse_pos) in
    begin match
        cell_contains, Lazy.from_fun Graphics.button_down, Lazy.from_fun Graphics.key_pressed, Lazy.from_fun Graphics.read_key
      with
      | None, lazy true, _, _ ->
        let cell = create_cell screen_mouse_pos (Random.run (List.random_choose strings)) in
        G.add_vertex graph cell;
        set_to_state Insert;
        true
      | _, _, lazy true, lazy 'e' -> set_to_state EdgeStart; true
      | _, _, lazy true, lazy 'i' -> set_to_state View; true
      | _ -> false
    end
  | View ->
    let screen_mouse_pos = Coordinate_system.from_display cs (s.mouse_x,s.mouse_y) in
    let cell_contains = Fun.flip G.iter_vertex graph |> Iter.find_pred (Fun.flip Cell.contains screen_mouse_pos) in
    begin match
        cell_contains, Lazy.from_fun Graphics.button_down, Lazy.from_fun Graphics.key_pressed, Lazy.from_fun Graphics.read_key
      with
      | Some cell, lazy true, _, _ -> set_to_state (Move (cell)); true
      | _, _, lazy true, lazy 'e' -> set_to_state EdgeStart; true
      | _, _, lazy true, lazy 'i' -> set_to_state Insert; true
      | _ -> false
    end
  | Move (cell) ->
    if Graphics.button_down () then begin
      let new_mouse_pos = Coordinate_system.from_display cs (s.mouse_x,s.mouse_y) in
      cell.x <- fst new_mouse_pos;
      cell.y <- snd new_mouse_pos;
    end else begin
      state := View
    end;
    true

let init : t =
  let graph = G.create () in    
  let c1 = (create_cell (10,10) "A piece of text") in
  let c2 = (create_cell (10,300) "Another piece of text") in
  G.add_vertex graph c1;
  G.add_vertex graph c2;
  G.add_edge graph c1 c2;
  (graph, ref View)

