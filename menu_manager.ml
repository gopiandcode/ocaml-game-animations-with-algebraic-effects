module Make
    (Menu: sig
       type t
       type message
       val update: t -> Graphics.status -> int -> message option
       val draw: t -> unit
       val draw_message: message -> unit
       val init: t
     end)  = struct
  type t = Menu.t * Menu.message list
  let _dim = 100

  let update (menu, lists) s time =
    match Menu.update menu s time with
    | None -> (menu, lists)
    | Some message -> (menu, message :: lists)

  let draw (menu, messages) =
    List.iter Menu.draw_message messages;
    Menu.draw menu

  let init = (Menu.init, [])

end
