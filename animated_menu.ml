type t = [ `New | `Load | `Settings | `Quit ] Generic_menu.t

type message = (string * int)

let init : t =
  let menu = Generic_menu.init () in
  Generic_menu.add "New Game" `New menu;
  Generic_menu.add "Load Game" `Load menu;
  Generic_menu.add "Settings" `Settings menu;
  Generic_menu.add "Quit Game" `Quit menu;
  menu

let pos = ref 100
let next_pos () = let vl = !pos in pos := !pos + 100; vl

let update t s time =
  let add_animation anim =
    let module M = struct type nonrec t = t let id = Generic_menu.id end in
    Animation_manager.add_animation (module M)  t anim in
  Generic_menu.update add_animation t s time
  |> Option.map (function
        `New -> ("Got New Game option", next_pos ())
      | `Load -> ("Got Load Game option", next_pos ())
      | `Settings -> ("Got Settings option", next_pos ())
      | `Quit -> ("Got Quit option", next_pos ()) )

let draw = Generic_menu.draw

let draw_message (str, pos) =
  Graphics.set_color Graphics.blue;
  Graphics.set_text_size 100;
  Graphics.moveto 500 pos;
  Graphics.draw_string str;
  
