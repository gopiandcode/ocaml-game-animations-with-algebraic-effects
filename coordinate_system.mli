type coords = int * int

type t = {x:int; y:int}

val from_world: t -> coords -> coords
val from_display: t -> coords -> coords
