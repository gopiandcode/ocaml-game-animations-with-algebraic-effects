open Containers
type state =
    Debounce
  | Static
  | Moving of {
      old_ind: int;
      old_brightness: int ref;
      current_brightness: int ref
    }

type 'a t = {
  id : Id.t;
  mutable options: (string * 'a)  list;
  mutable selected: int;
  mutable state: state;
} constraint 'a = [> ]

let id t = t.id

let init () = {options=[]; selected=0; id = Id.next_id (); state=Static;}

let add name vl t =
  t.options <- (name,vl) :: t.options

let change_index_anim t start stop = Animation.build @@ fun () -> 
  let open Animation in
  let old_brightness = ref 100 in
  let current_brightness = ref 0 in
  t.selected <- stop;
  t.state <- Moving {old_ind=start; old_brightness; current_brightness};
  run Transition.(
      Combine.in_parallel
        (InterpolateInt.between ~start:100 ~stop:0 ~delay:300
           (fun n -> old_brightness := n))
        (InterpolateInt.between ~start:0 ~stop:100 ~delay:300
           (fun n -> current_brightness := n)));
  t.state <- Static;
  return Transition.(Delay.of_ ~delay:0)

let debounce t = Animation.build @@ fun () -> 
  let open Animation in
  t.state <- Debounce;
  run @@ Transition.(Delay.of_ ~delay:10);
  t.state <- Static;
  return Transition.(Delay.of_ ~delay:0)

let update add_animation t _status _time =
  match Graphics.key_pressed (), t.state with
  | false,_ 
  | _, Debounce
  | _, Moving _ -> None
  | true, Static ->
    let key = Graphics.read_key () in
    let len = List.length t.options in
    match key with
    | 'p' -> add_animation (change_index_anim t t.selected ((t.selected + 1) mod len)); None
    | 'n' -> add_animation (change_index_anim t t.selected ((t.selected - 1 + len) mod len)); None
    | 's' -> add_animation (debounce t); Some (snd (List.nth t.options t.selected))
    | _ -> None

let width = 300
let height = 100
let offset_x = 100
let max_brightness = 100

let draw t =
  let y = ref ((Graphics.size_y ()) - 2 * height * (List.length t.options)) in
  let text_color vl = Graphics.rgb (170 + vl) 80 80 in
  let block_color vl = Graphics.rgb 0 (100 + vl) 80 in
  let get_color ind =
    let brightness = match t.state with
      | Static | Debounce ->
        if ind = t.selected
        then max_brightness
        else 0
      | Moving {old_ind; old_brightness; current_brightness} ->
        if ind = t.selected
        then !current_brightness
        else if ind = old_ind
        then !old_brightness
        else 0 in
    let text_color = text_color brightness in
    let block_color = block_color brightness in
    text_color, block_color
  in
  List.iteri (fun ind (name,_) ->
      let (text_color, block_color) = get_color ind in
      Graphics.set_color block_color;
      Graphics.fill_rect (offset_x - width/2) (!y - height/2) width height;
      Graphics.moveto offset_x !y;
      Graphics.set_color text_color;
      Graphics.draw_string name;
      y := !y + height * 2;
    ) t.options

