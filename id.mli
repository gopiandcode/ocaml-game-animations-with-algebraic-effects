type t = private int

val next_id : unit -> t
val hash: t -> int
val equal: t -> t -> bool
val compare: t -> t -> int
