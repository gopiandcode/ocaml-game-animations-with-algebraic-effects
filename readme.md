# Animations using algebraic effects in OCaml
This repository contains a series of examples of using algebraic
effects to implement game animations in OCaml.

## Files
The files:
```
animated_menu.ml
animated_graph.ml
animated_circle.ml
```
Correspond to each of the examples from the blog post.

## Setup
1. First, you'll need to switch to the ocaml-multicore branch of the compiler [See here](https://github.com/ocaml-multicore/multicore-opam#install-multicore-ocaml):
```
opam update
opam switch create 4.10.0+multicore --packages=ocaml-variants.4.10.0+multicore --repositories=multicore=git+https://github.com/ocaml-multicore/multicore-opam.git,default
```
2. Then, when on the multicore switch, install the required packages:
```
opam install graphics containers containers-data mtime ocamlgraph iter
```
3. Now you should be able to run the main function of the code with:
```
opam exec -- dune exec ./main.exe
```
