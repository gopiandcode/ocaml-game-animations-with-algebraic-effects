open Containers

type t = int
let count = ref 0
let next_id () = incr count; !count
let compare = Int.compare
let hash = Int.hash
let equal = Int.equal


