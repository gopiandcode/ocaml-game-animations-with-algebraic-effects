type s
type t

val run: ?on_cancellation:(unit -> unit) -> Transition.t -> unit
val return: Transition.t -> s

val build : (unit -> s) -> t
val update: int -> t -> t option
